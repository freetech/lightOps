package com.gsafety.devops.controller;
import com.gsafety.devops.entity.PortScanEntity;
import com.gsafety.devops.service.PortScanServiceI;
import com.gsafety.devops.util.PortScanner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.AjaxJson;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.core.constant.Globals;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.service.SystemService;
import org.jeecgframework.core.util.MyBeanUtils;

import java.io.OutputStream;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.entity.vo.NormalExcelConstants;
import org.jeecgframework.core.util.ResourceUtil;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.util.Map;
import java.util.HashMap;
import org.jeecgframework.core.util.ExceptionUtil;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**   
 * @Title: Controller  
 * @Description: 端口扫描
 * @author onlineGenerator
 * @date 2018-10-10 16:07:25
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/portScanController")
public class PortScanController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(PortScanController.class);

	@Autowired
	private PortScanServiceI portScanService;
	@Autowired
	private SystemService systemService;
	


	/**
	 * 端口扫描列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView list(HttpServletRequest request) {
		return new ModelAndView("com/gsafety/devops/portScanList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(PortScanEntity portScan,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		//执行端口扫描
		portScan();
		
		CriteriaQuery cq = new CriteriaQuery(PortScanEntity.class, dataGrid);
		//查询条件组装器
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, portScan, request.getParameterMap());
		try{
		//自定义追加查询条件
		
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.portScanService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	private void portScan() {
		List<PortScanEntity> portList = systemService.getList(PortScanEntity.class);
		for (PortScanEntity port : portList) {
			String result = PortScanner.scanPort(port.getIp(), Integer.valueOf(port.getPort()), 500);
			port.setStatus(result);
			port.setScanDate(new Date());
			systemService.updateEntitie(port);
		}
	}
	
	
	/**
	 * 删除端口扫描
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(PortScanEntity portScan, HttpServletRequest request) {
		String message = null;
		AjaxJson j = new AjaxJson();
		portScan = systemService.getEntity(PortScanEntity.class, portScan.getId());
		message = "端口扫描删除成功";
		try{
			portScanService.delete(portScan);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "端口扫描删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除端口扫描
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		String message = null;
		AjaxJson j = new AjaxJson();
		message = "端口扫描删除成功";
		try{
			for(String id:ids.split(",")){
				PortScanEntity portScan = systemService.getEntity(PortScanEntity.class, 
				id
				);
				portScanService.delete(portScan);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "端口扫描删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加端口扫描
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(PortScanEntity portScan, HttpServletRequest request) {
		String message = null;
		AjaxJson j = new AjaxJson();
		message = "端口扫描添加成功";
		try{
			portScanService.save(portScan);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "端口扫描添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新端口扫描
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(PortScanEntity portScan, HttpServletRequest request) {
		String message = null;
		AjaxJson j = new AjaxJson();
		message = "端口扫描更新成功";
		PortScanEntity t = portScanService.get(PortScanEntity.class, portScan.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(portScan, t);
			portScanService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "端口扫描更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 端口扫描新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(PortScanEntity portScan, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(portScan.getId())) {
			portScan = portScanService.getEntity(PortScanEntity.class, portScan.getId());
			req.setAttribute("portScan", portScan);
		}
		return new ModelAndView("com/gsafety/devops/portScan-add");
	}
	/**
	 * 端口扫描编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(PortScanEntity portScan, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(portScan.getId())) {
			portScan = portScanService.getEntity(PortScanEntity.class, portScan.getId());
			req.setAttribute("portScan", portScan);
		}
		return new ModelAndView("com/gsafety/devops/portScan-update");
	}
	
	/**
	 * 导入功能跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "upload")
	public ModelAndView upload(HttpServletRequest req) {
		req.setAttribute("controller_name","portScanController");
		return new ModelAndView("common/upload/pub_excel_upload");
	}
	
	/**
	 * 导出excel
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(params = "exportXls")
	public String exportXls(PortScanEntity portScan,HttpServletRequest request,HttpServletResponse response
			, DataGrid dataGrid,ModelMap modelMap) {
		CriteriaQuery cq = new CriteriaQuery(PortScanEntity.class, dataGrid);
		org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, portScan, request.getParameterMap());
		List<PortScanEntity> portScans = this.portScanService.getListByCriteriaQuery(cq,false);
		modelMap.put(NormalExcelConstants.FILE_NAME,"端口扫描");
		modelMap.put(NormalExcelConstants.CLASS,PortScanEntity.class);
		modelMap.put(NormalExcelConstants.PARAMS,new ExportParams("端口扫描列表", "导出人:"+ResourceUtil.getSessionUser().getRealName(),
			"导出信息"));
		modelMap.put(NormalExcelConstants.DATA_LIST,portScans);
		return NormalExcelConstants.JEECG_EXCEL_VIEW;
	}
	/**
	 * 导出excel 使模板
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(params = "exportXlsByT")
	public String exportXlsByT(PortScanEntity portScan,HttpServletRequest request,HttpServletResponse response
			, DataGrid dataGrid,ModelMap modelMap) {
    	modelMap.put(NormalExcelConstants.FILE_NAME,"端口扫描");
    	modelMap.put(NormalExcelConstants.CLASS,PortScanEntity.class);
    	modelMap.put(NormalExcelConstants.PARAMS,new ExportParams("端口扫描列表", "导出人:"+ResourceUtil.getSessionUser().getRealName(),
    	"导出信息"));
    	modelMap.put(NormalExcelConstants.DATA_LIST,new ArrayList());
    	return NormalExcelConstants.JEECG_EXCEL_VIEW;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(params = "importExcel", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importExcel(HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			ImportParams params = new ImportParams();
			params.setTitleRows(2);
			params.setHeadRows(1);
			params.setNeedSave(true);
			try {
				List<PortScanEntity> listPortScanEntitys = ExcelImportUtil.importExcel(file.getInputStream(),PortScanEntity.class,params);
				for (PortScanEntity portScan : listPortScanEntitys) {
					portScanService.save(portScan);
				}
				j.setMsg("文件导入成功！");
			} catch (Exception e) {
				j.setMsg("文件导入失败！");
				logger.error(ExceptionUtil.getExceptionMessage(e));
			}finally{
				try {
					file.getInputStream().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return j;
	}
	
	
}
